# Interaction in Mixed Reality Spaces: Exercise 6

**Issue Date:** 24.06.2021 

**Due Date:**  22.07.2021, 09:00

**Achieved Points**: 	Project: **[25/30]**, Presentation: **[30/30]**,	Total: **[55/60]**

## 1. Project (30 Points)

In this part of exercise 6 you are supposed to implement the second half of your final design concept with unity 3d, to document your work progress, and to document your final state. You are allowed to use everything that you created during the exercises 1-5 as a basis. Please follow the instructions below: 

*a. Implementation **[9/10&nbsp;Points]***

+ You are required to implement at least  **four** interaction techniques or cues until the *Presentation of the final app state*, which you did not implement as part of the *implementation part* in the exercises 1-4! Note, that you already should have implemented at least **two** of the minimum **four** techniques or cues by now (as part of exercise 5).  
+ Submit the code for your final implementation to the Gitlab (make sure that your code can be easily executed on a Quest 2 and runs without errors).
+ Also submit an apk for your implementation that can be installed on the Quest 2  to the Gitlab.
+ Document the c# code in your project.

*Nice, but sometimes affected by unexpected behavior.*

*b. Documentation of Work Progress **[8/10&nbsp;Points]***
+ Document your work progress in the readme file, below.
    - If your app requires a certain setup, make sure to explain how the setup is done (e.g., by placing markers).
    - Document how you implemented different interaction techniques and cues after the presentation of the intermediate app state. This concerns:
        - Interaction techniques for 3d manipulation.
        - Travel techniques and wayfinding cues.
        - Interaction techniques for system control and symbolic input.                  
    - Document your work progress in a logical order. 
    - Provide arguments for important design decisions (why did you decide for option A instead of option B?).
    - Document the problems you ran into and how you solved/addressed them (see the instructions for the creation of your final presentation for details).
+ You can include images, videos, and code fragments to illustrate parts of your documentation.
+ As mentioned before, you are permitted to update your concept as part of your project work if this should be necessary (for example, because of technical limitations). If you made important changes to your concept as part of this exercise, please briefly clarify how and why.

------
Overview:

The first kitchen was designed with an old asset. Accordingly, the first scenes consisted of an older asset. However, these were adjusted at the end.

<img src="Documentation/firstStorage.PNG" alt="first" width="550"/>


For teleportation and the Magic Spoon, simple planes and cubes were used first to ensure functionality before adding the associated objects.

<img src="Documentation/startSpoon.PNG" alt="startP" width="550"/>

<img src="Documentation/startTeleport.PNG" alt="startT" width="550"/>



Interaction techniques for 3d manipulation: 

1. Interact with doors/drawers:

For the interaction with the kitchen environment I've used Joints. For drawers a Configurable Joint and for Doors a Hinge Joint.

Drawers:

Additional to the Box Collider a Configurable Joint was added to the drawer object. All Motions besides the X Motion were set on locked, so that the drawer could only move in one direction. Furthermore a limit was set, so that the drawer movement is restricted.

<img src="Documentation/CFJoint.PNG" alt="drawer" width="550"/>



Doors:

For opening doors a hinge joint was used. A first problem occured when the door was accidently set on static. This interfered with the opening, which resulted into a whole door grabbed and attached to the controller. Like the Configurable Joint the Hinge Joint was set to limeted an the angular limits were edited for each door.

<img src="Documentation/hingeJoint.PNG" alt="door" width="550"/>


2. Cut Objects:


Instead of dividing the mesh of an object I've used the trick of minimizing the original object and instantiating a sliced object next to the original one.


3. Destroy Objects:

For destroying objects, I've created a trash can, consiting of planes and cubes. Inside the trash can a plane waits for detecting collisions. If an object collides with the plane, it will get destroyed.


Travel techniques and wayfinding cues:

1. Teleportation by magical spoon:

Therefore I started using a simple cube. To that I've added a script (DoorSwitch.cs) as well as a LineRenderer. The scipt checks if the raycast collides with another object. Therefore I checked if the raycast collides with any door/plane. If that is the case a logical AND Link between pushing the trigger and the collision can be used to enable the scene switch. After that worked I've added the same to an existing spoon asset. First, the raycast directed in the wrhong direction. Since I couldn't change that on the prefab, I've added a little cube at the front of the spoon and linked it with the raycast.

2. Wayfinding Cues:

At the beginnign the player needs to find the task board and the recipe.

<img src="Documentation/recipe.PNG" alt="recipe" width="550"/>

I've added sound/ an audio source when the knives get touched for the first time. This indicates that these knives aren't suitable for cutting and the player need to find the right one. Therefore he will be routed by spatial sound to finde the right drawer in time.

<img src="Documentation/spatialSound.PNG" alt="spatialSound" width="550"/>

Another cue is the higlighted outline of the far away object on the shelf. Therefore, the user gets directly feedback if he has selected the right gameObject.

<img src="Documentation/outline.PNG" alt="outline" width="550"/>




Interaction techniques for system control and symbolic input:

1. System Control:

At the beginning the user can edit inputs of the system. Since it has already done for the last assignment and I hadn't time to complete it this part is unchanged.



I've started to provide a timer, which shows the remained time (Timer.cs). Due to the time, I wasn't able to finsih it.

*Ok, but could be more clear. Especially, the information on how parts of your implementation stand in relation to your concept, and the description of design decisions and how you tackled ocurring problems is limited (a bit more details and context information would have been appropriate).*

------

*c. Documentation of the Final Concept and App State **[8/10&nbsp;Points]***
+ Document your final concept and app state in the readme file, below.
    - This section of the documentation is supposed to show your final results (for the work progress, see b). 
    - Give an overview on the different interaction techniques and cues that you implemented in your final app state. This concerns:
        - Interaction techniques for 3d manipulation.
        - Travel techniques and wayfinding cues.
        - Interaction techniques for system control and symbolic input.                  
    - Provide a visual walkthrough through your final concept and app state (you may make use of screen captures - i.e., videos and images - and content from your original storyboard to clarify your final state). Highlight how you support the user with different interaction techniques and cues when he or s/he uses your app.  
    - If your app requires a certain setup, make sure to explain how the setup is done (e.g., by placing markers).

------
Overview:

The Cooking adventure consits of 3 scenes/rooms. 

1. Cooking Room:

<img src="Documentation/kitchen.PNG" alt="kitchen" width="550"/>

2. Storage:

<img src="Documentation/storage.jpg" alt="storage" width="550"/>

3. Service:

<img src="Documentation/service.jpg" alt="service" width="550"/>







Interaction techniques for 3d manipulation: 

In the Cooking Scene it is possible to interact with the environment. This means that drawers and doors can be opened, or objects such as dishes can be moved. 

1. Interact with doors/drawers:

Drawers:

<img src="Documentation/openDrawer.PNG" alt="drawer" width="550"/>

Doors:

<img src="Documentation/hingeJoint.PNG" alt="hinge" width="550"/>

<img src="Documentation/openFridge.PNG" alt="fridge" width="550"/>

2. Grab Objects:

<img src="Documentation/grabSpoon.PNG" alt="spoon" width="550"/>

<img src="Documentation/getKnife.PNG" alt="knife" width="550"/>

3. Cut Objects:

<img src="Documentation/cutBread.PNG" alt="cut" width="550"/>


4. Destroy Objects:

<img src="Documentation/destroy.PNG" alt="destroy" width="550"/>

5. Grab far objects:

With the help of the Magic Spoon, distant objects can be drawn in. As soon as the raycast collides with the object, it is highlighted in color. If the A-Btton is also pressed, the object is positioned 5cm in front of the user using the MoveTowards() method.

<img src="Documentation/raycast.PNG" alt="grab" width="550"/>

6. End Game:

<img src="Documentation/end.jpg" alt="hinge" width="550"/>


Travel techniques and wayfinding cues:

1. Teleportation by magical spoon:

<img src="Documentation/grabSpoon.PNG" alt="spoon" width="550"/>

2. Wayfinding Cues:

I've added sound/ an audio source when the knives get touched for the first time. This indicates that these knives aren't suitable for cutting and the player need to find the right one. Therefore he will be routed by spatial sound to finde the right drawer in time.

Another cue is the higlighted outline of the far away object on the shelf. Therefore, the user gets directly feedback if he has selected the right gameObject.

<img src="Documentation/outline.PNG" alt="outline" width="550"/>


Furthermore, hints are provided with the painting on the wall for the usage of the magical spoon.

<img src="Documentation/painting1.PNG" alt="painting1" width="550"/>

<img src="Documentation/painting2.png" alt="painting2" width="550"/>

3. Drop Bread in Basket:

<img src="Documentation/dropBread.PNG" alt="bread" width="550"/>

4. Task Overview:

<img src="Documentation/tasks.jpg" alt="tasks" width="550"/>

Interaction techniques for system control and symbolic input:

1. System Control:

At the beginning the user can edit inputs of the system. Since it has already done for the last assignment and I hadn't time to complete it this part is unchanged.

<img src="Documentation/start.PNG" alt="canvas" width="550"/>

*Ok, but the visual walkthrough could have been more clear. The descriptions are rather short and would be easier to follow if more context information would have been provided. The purpose of figure 6 "End Game" is not  clear at all.*

------

## 3. Presentation (30/30 Points)

The presentation  of your final concept and app state will be done in form of a video presentation, and live. First, you will need to conduct a video recording of your presentation and upload it to ILIAS until the deadline on the due date (22.07.2021, 09:00). Second, you are also invited to meet with us in the Mediaroom (Z924) were you can connect your notebook to the Surface Hub and present your final concept and app state live (in case live presentations are not possible due to changes in regard to the Corona regulations, we will inform you beforehand). The live presentations are scheduled to start at the regular begin of the lecture (22.07.2021, 15:15). As part of the live presentations, we kindly ask you to also bring your Quest 2 HMD and to prepare it for a live demo of your app (we have a cleanbox in the Mediaroom that we can use to clean the devices before trying your app). 

### Some things that are important:  
+ The total time for presentation is maximum **nine** minutes per person! Each presenter will be strictly held to the time limit. If you go over, your grade will be affected.
+ Your presentation should look professional, prepared with presentation software (e.g.,  PowerPoint). 
+ Make all information on the slides well perceivable (avoid tiny images that no one can understand and small text sizes that make text unreadable).
+ Number your slides. 
+ Avoid it to speak to fast and speak clearly. 
+ Avoid overloaded slides and reduce the amount of text to the minimum necessary to convey the information that is essential. 
+ You can find the grading sheet for the presentation for download in ILIAS. 

### Presentation Structure (Your tasks):

The focus of the presentation is your final concept and app state. Your presentation should address the following points: 

*a. Introduction  [approximately 1 minute]*
+ Title slide (your name, topic, matricle number, etc).
+ Briefly re-introduce the topic of  your project.
+ Give a brief overview on your final app state. As part of this, state which interaction techniques and cues you implemented in your final app (techniques for 3d manipulation, travel, system control, symbolic input, and wayfinding cues).

*b. Final Concept and App State [approximately 7 minutes]*
+ Present your final concept and app state in a logical order. Tell a story! You may make use of your original storyboard here (for example, you can make use of some sketches from your storyboard to frame the presentation of your final concept and app state. You could use sketches to show the initial situation of a user before your app is used, an intermediate situation, or the conclusion of the use of your app). 
+ If you changed the concept in contrast to your original concept (e.g., for technical reasons), communicate these changes and also mention why you made changes to your concept.
+ Properly narrate and illustrate interactions, so that the viewers of your presentation can easily understand what is happening (e.g., if a button is pressed, an object is rotated, etc.)
+ Make sure that your narration is consistent with what you show (e.g., avoid it that a video goes on, while you still talk about something that is no longer shown in the video).
+ If your app requires a certain setup, briefly mention how the setup is done (e.g., by placing markers).
+ <b>(Very Important!)</b> Make sure to highlight how users can interact when they use the app and how they are supported by cues. This concerns:
    - Interaction techniques for 3d manipulation.
    - Travel techniques and wayfinding cues.
    - Interaction techniques for system control and symbolic input.
+ Provide arguments for important design decisions (why did you decide for option A instead of option B?).
    
 *c. Reflection and Outlook [approximately 1 minute]*   
+ What were the main challenges during the project and how were they addressed? (includes technical and conceptual challenges!)
+ How could your application be improved in the future? 
+ What would you do different if you had to start again from scratch?

The structure above provides a suggestion that you can use as a template for the structuring of your own presentation. You may have to adapt it to live within the nine-minute limit.  You must practice your presentation several times to get a smooth and interesting talk and keep within your time limit.

### Presentation Submission

*a. PDF of presentation*
+ Submit your presentation slides in ILIAS in form of a single pdf file into the folder: [https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250568.html](https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250568.html) (*III. Presentation of Final App State/Presentations (Student Submissions)*). 
+ Name the file (in this order, please): `“<course name> - <current semester>_<year> - final state - <your name>.pdf”`.  If you do not follow the naming schema provided, your grade will be affected.

*b. MP4 Video of presentation*
+ Submit your video in ILIAS in form of a single mp4 file into the folder: [https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250569.html](https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250569.html) (*III. Presentation of Final App State/Videos (Student Submissions)*).
+  Name the file (in this order, please): `“<course name> - <current semester>_<year> - final state - <your name>.mp4”`.  If you do not follow the naming schema provided, your grade will be affected. 
+ You can directly upload videos <= 256 MB to ILIAS. If this should not work, you may upload your video to the nextcloud of the university ([https://cloud.uni-konstanz.de](https://cloud.uni-konstanz.de)) and add a link to your video in ILIAS.
+ For the creation of a remote presentation you may have a look at the following guide (you do not need subtitles!): [href=https://medium.com/sigchi/a-remote-video-presentation-guide-93957c63aa7a](https://medium.com/sigchi/a-remote-video-presentation-guide-93957c63aa7a)
+ We recommend you the use of a resolution of 1920x1080.
+ To include a video of the presenter on your slides is optional (you are free to do so, but it is not a must have). Powerpoint (Office 365) supports this out of the box now: [https://support.microsoft.com/en-us/office/record-a-slide-show-with-narration-and-slide-timings-0b9502c6-5f6c-40ae-b1e7-e47d8741161c#OfficeVersion=Microsoft_365_for_Windows](https://support.microsoft.com/en-us/office/record-a-slide-show-with-narration-and-slide-timings-0b9502c6-5f6c-40ae-b1e7-e47d8741161c#OfficeVersion=Microsoft_365_for_Windows).
+ To decrease the size of your video before its upload you can use handbrake: [https://handbrake.fr](https://handbrake.fr).
