using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceGrab : MonoBehaviour
{
    public GameObject cameraRig;
    public GameObject rightControllerAnchor;

    // Update is called once per frame
    void Update()
    {

        Vector3 rightPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

        Vector3 camPos = cameraRig.transform.position;

        float distance = Vector3.Distance(camPos, rightPos);

        Debug.Log(distance);

        if (distance > 0.3f)
        {

            rightControllerAnchor.transform.position += rightControllerAnchor.transform.forward * 0.5f;
        }
        
    }
}
