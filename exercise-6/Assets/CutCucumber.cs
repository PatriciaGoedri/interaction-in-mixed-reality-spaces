using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutCucumber : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject vegetable;

    public GameObject slice;


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Knife")
        {
           // vegetable.transform.localScale -= new Vector3(0,0,0.3f);
            Instantiate(slice, vegetable.transform.position + 0.5f * vegetable.transform.forward, Quaternion.identity);
        }
        
    }
}
