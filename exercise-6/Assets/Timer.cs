using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    
    public float timeRemaining = 10;
    public bool timerRunning = false;

    float minutes;

    float seconds;

    public Text timeText;


    void Start()
    {
        timerRunning = true;
    }

    void Update()
    {
        

        if(timerRunning)
        {
            if (timeRemaining > 0)
            {
            timeRemaining -= Time.deltaTime;
            }
            else
            {
            Debug.Log("time over");
            timeRemaining = 0;
            timerRunning = false;
            }
        }
        
    }

    void ShowTime(float showTime)
    {

        timeRemaining += 1;
        
        minutes = Mathf.FloorToInt(timeRemaining / 60);

        seconds = Mathf.FloorToInt(timeRemaining % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
