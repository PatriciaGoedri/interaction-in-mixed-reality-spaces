using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportObject : MonoBehaviour
{
    public string scene;
    public GameObject bread;

    public GameObject tool;

    public LineRenderer lineRenderer;


     void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(tool.transform.position, tool.transform.TransformDirection(Vector3.forward), out hit))
        {
             lineRenderer.SetPositions(new Vector3[]{tool.transform.position, hit.point});
             

             

    
        } else {
            // Nothing hit, set length of line to 5 meters
            lineRenderer.SetPositions(new Vector3[]{tool.transform.position, tool.transform.position + 5 * tool.transform.forward});
        }


        if (hit.collider.name == "Cooking" && OVRInput.Get(OVRInput.Button.One))
        {
            StartCoroutine(LoadYourAsyncScene());
        }
    }

     IEnumerator LoadYourAsyncScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();

        // The Application loads the Scene in the background at the same time as the current Scene.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);

        // Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        // Move the GameObject (you attach this in the Inspector) to the newly loaded Scene
        SceneManager.MoveGameObjectToScene(bread, SceneManager.GetSceneByName(scene));
        // Unload the previous Scene
        SceneManager.UnloadSceneAsync(currentScene);
    }
}
