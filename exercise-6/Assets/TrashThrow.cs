using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashThrow : MonoBehaviour
{
    public GameObject vegetable;
    
    int counter;

    bool point = false;
    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Trash")
        {

            point = true;

            Destroy(vegetable);

        }
        
    }

    void Update()
    {
        if(point)
        {
            counter += 1;
            point = false;
        }
    }
}
