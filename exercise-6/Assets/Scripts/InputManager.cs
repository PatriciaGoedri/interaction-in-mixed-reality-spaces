﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public GameObject cubePrefab;
    public GameObject cameraRig;
    public GameObject rightControllerAnchor;

    public LineRenderer lineRenderer;


    // Update is called once per frame
    void Update()
    {

        // Check if right trigger button was pressed this frame
        if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
        {
            // If button was pressed, instantiate cube and the controller position
            Instantiate(cubePrefab, cameraRig.transform.position + 0.5f * cameraRig.transform.forward, Quaternion.identity);

            //OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)
        }

        RaycastHit hit;
        // Check if the ray intersects with any object
        if (Physics.Raycast(rightControllerAnchor.transform.position, rightControllerAnchor.transform.TransformDirection(Vector3.forward), out hit))
        {
            // Shorten the ray of the line renderer
            lineRenderer.SetPositions(new Vector3[]{rightControllerAnchor.transform.position, hit.point});
        } else {
            // Nothing hit, set length of line to 5 meters
            lineRenderer.SetPositions(new Vector3[]{rightControllerAnchor.transform.position, rightControllerAnchor.transform.position + 5 * rightControllerAnchor.transform.forward});
        }


        // Check if the A button is pressed
        if (OVRInput.Get(OVRInput.Button.One))
        {
            hit.transform.Rotate(new Vector3(0, 0.5f, 0));
        }

        // Check if the B button was pressed this frame
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            Destroy(hit.transform.gameObject);
        }
    }


}
