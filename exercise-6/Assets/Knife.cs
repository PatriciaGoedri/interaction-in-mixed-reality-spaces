using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public AudioClip knifeIntro;


    void Start()
    {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = knifeIntro;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("KNIFE");
        GetComponent<AudioSource>().Play();
    }
}
