using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutBread : MonoBehaviour
{
    public GameObject bread;

    public GameObject breadSlice;


/*
    void Update()
    {

        if(OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.Touch) > 0.3f && bread)
            {
                ScaleCube();
            }
    }
*/

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Knife")
        {
            //bread.transform.localScale -= bread.transform.localScale - new Vector3(0,0,0.3f);
            Instantiate(breadSlice, bread.transform.position + 0.5f * bread.transform.forward, Quaternion.identity);
        }
        
    }


    public void ScaleCube()
    {
        Vector3 leftPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        Vector3 rightPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

        float distance = Vector3.Distance(leftPos, rightPos);

        //this.transform.localScale = Vector3.one * distance;
        bread.transform.localScale = Vector3.one * distance;
    }
}
