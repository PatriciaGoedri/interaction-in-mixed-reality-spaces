using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalButton : MonoBehaviour
{
    public Color finishColor;
    public Color startColor;

    public Transform top;

    private Material[] materials;
    

    public AudioClip ending;

    void Start()
    {
        
        materials = new Material[] {top.GetComponent<MeshRenderer>().material};
        setColor(startColor);

        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = ending;
    }


    void OnCollsionEnter(Collision other)
    {
        Debug.Log("push");
        top.transform.position -= Vector3.down * 0.02f;
        setColor(finishColor);
        GetComponent<AudioSource>().Play();

    }

    private void setColor(Color color){
        foreach (var material in materials)
        {
            material.color = color;
        }
    }
}
