using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCast : MonoBehaviour
{
   
    public float radius;
    public float maxDistance;

    public LayerMask layerMask;

    private Vector3 origin;
    private Vector3 direction;

    public GameObject rightControllerAnchor;

    public GameObject currentObject;

    private float currentDistance;

    public SphereCast sphereCast;

    void Update()
    {
        Vector3 bread = transform.position;
        Vector3 rightPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
        RaycastHit hit;


        if(Physics.SphereCast(rightPos, 0.8f, rightControllerAnchor.transform.forward , out hit, 0.5f, layerMask, QueryTriggerInteraction.UseGlobal))
        {
            currentObject = hit.transform.gameObject;

            Debug.Log("Object hit");

            currentObject.GetComponent<Outline>().enabled = true;

            currentDistance = hit.distance;

            if(OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger, OVRInput.Controller.Touch) > 0.3f && currentObject)
            {
                float step = 0.2f * Time.deltaTime;
                currentObject.transform.position = Vector3.MoveTowards(transform.position, rightPos, step);
            }
        
        }
        else {
            currentDistance = 0.5f;
            currentObject = null;
        }
    }

    
}
