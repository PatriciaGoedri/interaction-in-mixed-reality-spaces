using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorSwitch : MonoBehaviour
{
    public GameObject UIRootObject;
    public GameObject tool;

    public LineRenderer lineRenderer;

    public GameObject bread;

    public GameObject currentObject;


/*
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Bread");

        DontDestroyOnLoad(this.gameObject);
    }
    */
    void Update()
    {

        RaycastHit hit;

        if (Physics.Raycast(tool.transform.position, tool.transform.TransformDirection(Vector3.forward), out hit))
        {
             lineRenderer.SetPositions(new Vector3[]{tool.transform.position, hit.point});
             

             

    
        } else {
            // Nothing hit, set length of line to 5 meters
            lineRenderer.SetPositions(new Vector3[]{tool.transform.position, tool.transform.position + 5 * tool.transform.forward});
        }
        
    

        if (hit.collider.name == "Storage")
        {
            Debug.Log("Let's move to the storage");

            
        
                if (OVRInput.Get(OVRInput.Button.One))
                {
                    
                    Debug.Log("yaaaaay");
                    //SceneManager.MoveGameObjectToScene(UIRootObject, scene);
                    //SceneManager.SetActiveScene(scene);
                    SceneManager.LoadScene("Storage");
                }
        }

        if (hit.collider.name == "Cooking")
        {
            Debug.Log("Let's move to the Cooking place");
        
                if (OVRInput.Get(OVRInput.Button.One))
                {
                    
                    Debug.Log("yaaaaay");
                    SceneManager.LoadScene("Cooking");
                }
        }

        if (hit.collider.name == "Service")
        {
            Debug.Log("Let's move to the storage");
        
                if (OVRInput.Get(OVRInput.Button.One))
                {
                    
                    Debug.Log("yaaaaay");
                    SceneManager.LoadScene("Service");
                }
        }

         if (hit.collider.name == "Bread")
         {
            bread.GetComponent<Outline>().enabled = true;
            if (OVRInput.Get(OVRInput.Button.One))
            {
                float step = 0.2f * Time.deltaTime;
                bread.transform.position = Vector3.MoveTowards(transform.position, tool.transform.position + 0.5f * tool.transform.forward , step);
            }
         }

        


    }
}
