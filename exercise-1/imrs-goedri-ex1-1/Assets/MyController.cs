using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyController : MonoBehaviour, IPointerClickHandler
{
    public float movement = 5.0f;
    public GameObject cube;

    OVRCameraRig oculusCam;

    //Vector3 controllerPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

    /*
    void Start()
    {
    Debug.Log("Start");
    //Instantiating test
    GameObject cubeClone1 = Instantiate(cube,  new Vector3(3,5,0) , Quaternion.identity);
    }
    */
    void Update()
    {
        //Debug.Log(OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger));

        //controllerPos += transform.forward * movement;

         if(OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
        {
            //Debug.Log("Trigger");

            Vector3 currentPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

            

            GameObject cubeClone = Instantiate(cube, currentPos + new Vector3(0,0,0.5f), Quaternion.identity);
        }

        
        /*
        //Raycast easily integrated with Oculus prefab (UIHelpers)

        

        Ray ray = oculusCam.ScreenPointToRay(OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {

        }*/


    }
        
         //When Controller collides with Instantiate Block
  void OnTriggerEnter(Collider other)
  {
    //Debug.Log("Collison"); 
    //if (other.gameObject.name == "ToyCubePf(Clone)")
    // {
      OVRInput.SetControllerVibration(1f,1f,OVRInput.Controller.RTouch);
      other.GetComponent<Renderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
    // }
  }

  public void OnPointerClick(PointerEventData pointerEventData)
  {
      
       //rotation y-axis
    if(OVRInput.Get(OVRInput.Button.One) )
    {
      cube.transform.Rotate(new Vector3(0,0.5f,0));
    }
    //deleting cubeClone
    if(OVRInput.GetDown(OVRInput.Button.Two))
    {
      Destroy(cube);
    }
  }
     
}
