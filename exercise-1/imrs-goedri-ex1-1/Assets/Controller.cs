using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    
    public GameObject cube;


    void Start()
  {
      Debug.Log("Start");
    //Instantiating test
    GameObject cubeClone1 = Instantiate(cube,  new Vector3(1,1,1) , Quaternion.identity);
  }


    // Update is called once per frame
    void Update()
    {

        //OVRInput.Update();

        //Debug.Log(OVRInput.Get(OVRInput.Button.Any));
        //Debug.Log(OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger));
        
        if(OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            Debug.Log("Trigger3");
            GameObject cubeClone = Instantiate(cube, new Vector3(100,100,0), Quaternion.identity);
        }
        //Debug.Log("Update");
        
        if(OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger))
        {
            Debug.Log("Trigger2");
            GameObject cubeClone = Instantiate(cube, new Vector3(100,100,0), Quaternion.identity);
        }

        if(OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
        {
            Debug.Log("Trigger");
            GameObject cubeClone = Instantiate(cube, new Vector3(100,100,0), Quaternion.identity);
        }
        
    }
}
