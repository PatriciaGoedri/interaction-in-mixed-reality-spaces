# Interaction in Mixed Reality Spaces: Exercise 1

**Issue Date:** 15.04.2021

**Due Date:**  29.04.2021, 09:00

**Achieved Points**: 	Topic Selection: **[1/1]**, 	Project Setup: **[2/2]**, 	Unity Basics: **[7/7]**,	Total: **[10/10]**



## 1. Topic Selection (1 Point)

As part of this lecture you will work on a semester long project. To support you in finding a topic for your project, we presented different project topics to you as part of the first lecture session (see the uploaded slides in ILIAS). You can choose any of the presented topics for your own project. However, please note that each topic can only be covered by maximum two persons. The assignment is based on a first come first serve basis (if multiple persons propose to work on the same topic, the topic goes to the two persons who ask first).

Each participant in the lecture must get its choice of project topic approved by the instructors as early as possible (before exercise 1 is due). To simplify the process of the topic assignment, we kindly ask you to send us a mail with your top-3 topics to maximilian.duerr@uni.kn.

**For example:**  
*First Priority: #2 I am starving: The cooking adventure  
Second Priority: #1 Let’s make me rich: The safe robbery  
Third Priority: #3 Far from home: The builder of star constellations*  

Please use the SAME formatting for your top-3 topics in your mail, as in the example above.

If you have an idea for a project topic by yourself, it is also possible that you propose your own topic to get it approved. If this should be the case, please also send a mail with your topic idea (including the main unity 3d object and a brief scenario description) to maximilian.duerr@uni.kn.

Briefly document your approved topic choice, including the main unity 3d object and a brief scenario description.

------

*Documentation area*

The selected project topic is **I am starving: The cooking adventure**. The main unity 3d object consists of a kitchen. The aim is to develop a small game in which the cook has to prepare a series of ingredients. Interactions such as cutting, transporting and cooking (e.g. swirling the pan) of ingredients will be included. In addition, different tools will be available for the respective action.





------


## 2. Project Setup (1 Point)

For the practical project you will use the game engine Unity3D to develop an Android app for the Oculus Quest 2. Follow the following instructions, to setup your development environment.

*Optional: If your hardware is powerful enough, you can also use a link cable for development and run the application on your PC instead of creating an Android application. For further information see: https://support.oculus.com/394778968099974/?locale=en_US. Please contact us, if you are interested in using the link cable. The basic setup then also differs from the instructions below .*  

**a) Install Unity**. At first you need to download and install Unity:
1. Download and install Unity Hub: https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe
2. Open Unity Hub, switch to the **Installs** tab on the left, click **Add** and select **Unity 2020.3.3f1 (LTS)** from the list, then click **Next**.
3. On the **Add modules to your install** page select and expand **Android Build Support** and make sure that also **Android SDK & NDK Tools** and **OpenJDK** are selected.
4. Click on **Next**, agree the **End User License Agreement**, and click on **Done**.
5. Wait for the installation to be completed.

**b) Create a Unity Project.** With the installation finished, your next task is to create a Unity Project for your application:
1. In Unity Hub, switch back to the **Projects** tab on the left.
2. Click on **New** (if you have multiple Unity installations on your machine, click on the arrow in the left of the new button and select **Unity 2020.3.3f1 (LTS)**).
3. Enter a project name (e.g., `imrs-wieland-ex1`) and select the folder `exercise-1` in your checked out repository as the location for your project, then click on **Create**.
4. Wait until Unity created you project and the Unity Editor is open.

**c) Configure the Build Settings.** With the project open it is now time to configure your project. At first, the build settings:
1. Go to **File > Build Settings** in the menu of Unity Editor
2. Under **Platform**, select **Android**
3. Set **Texture Compression** to **ASTC**
4. In **Run Device** select your Oculus Quest 2 (which is only in the list if you connect the headset via USB to your machine)
5. Click **Switch Platform**

**d) Configure the Project Settings.** The next step is to configure the project settings:
1. Go to **Edit > Project Settings > Player** in the menu of Unity Editor
2. In **Company Name** type e.g. `hcikn`
3. In **Product Name** type the name of your app, e.g., `Safe Robbery`
4. In the **Android** tab expand **Other Settings**
5. In **Minimum API Level**, set the minimum Android version to Android 6.0 Marshmallow (API level 23)
6. In **Target API Level**, make sure that **Automatic (highest installed)** is selected
7. In **Install Location**, select **Automatic**

 **e) Enable Virtual Reality (VR) Support.** To build your VR app, you need to enable the VR support in Unity.
1. Go to **Edit > Project Settings** in the menu of Unity Editor.
2. Select **XR Plugin Management** and click **Install XR Plugin Management**
3. Wait until the XR Plugin Management is installed, then select **Oculus** to install the Oculus XR plugin

**f) Add Oculus Integration.**
1. Go to **Window > Asset Store** and search for **Oculus Integration**, install the asset and import it into you project.
2. Create a new Scene in the Project Window (e.g. `ex-1`) and open it: **Right click in the Scenes folder in the project window > Create > Scene**, name it, double click it.
3. Delete the **Main Camera** in the Hierarchy window.
4. In the Project window navigate to **Assets > Oculus > VR > Prefabs**. Drag the `OVRCameraRig` prefab into the Hierarchy.
5. To convert the main camera to a XR Rig, perform a right click in the Hierarchy window, then **XR > Convert Main Camera to XR Rig**.
6. Expand the `OVRCameraRig` object in the Hierarchy. Expand all objects inside the `OVRCameraRig`.
7. Add the `OVRControllerPrefab` from **Assets > Oculus > VR > Prefabs** to both the `LeftControllerAnchor` and also to the `RightControllerAnchor` of your `OVRCameraRig`.
8. Click on the `OVRControllerPrefab` in the `LeftControllerAnchor` to see it in the Inspector window. Under **Controller** in the `OVRControllerHelper` skript, select `LTouch`. Repeat the step for the `OVRController Prefab` in the `RightControllerAnchor`. This time select `RTouch`.

**g) Build your project to the Quest 2 and test it.** Time to build your project to the device and test if you did everything right.
1. Connect your Oculus Quest 2 to your PC via USB (if not already done). Make sure that you have allowed USB Debugging on the HMD. You are asked if you want to allow it, when you connect the HMD to your PC.
2. Navigate to **File > Build Settings** in the menu of the Unity Editor and click on **Add Open Scenes**. Make sure, that only the scene that you created in step e) is checked in **Scenes in Build**.
3. In the menu of the Unity Editor navigate to **Oculus > OVR Build > OVR Build APK and Run**.
4. When the scene is deployed to the device, the project should start on the HMD and you should be able to look around in Unity's default scene and see the controllers in your hands.

**h) Save, Commit and Push your Project.** With the previous steps completed, it is time to save (CTRL/CMD+S), commit (Commit Message `Project Setup`), and push your project to your Gitlab repository (folder: `exercise-1`).


## 3. Unity Basics (8 Points)

The aim of this task is, that you familiarize yourself with Unity Development using C# Scripts. Unity provides very good learning material if you are new to Unity development (https://learn.unity.com/ and https://docs.unity3d.com/ScriptReference/). Additionally, have a look at the Oculus specific documentation (https://developer.oculus.com/unity/). Based on these resources implement the following interactions:

**a)** When pressing the trigger button of the controller, a cube (10x10x10cm) is spanned 0.5 meters in front of the user. **[1 Point]**  
Implementation hints:
  - Listen for controller button presses in the `Update()` method
  - `OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger)` returns true if the trigger button of the right controller was pressed in the current frame
  - Use `OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)` to get the current position of the right controller
  - Use a prefab for the cube and the method `GameObject.Instantiate()` to create an instance of this prefab during runtime

**b)** When touching a cube with one of the controllers, the controller vibrates shortly and the cube's color is changed to a different random color. **[3 Points]**  
Implementation hints:
  - Add a `RigidBody` and a `Box Collider` to your cube prefab
  - Enable `IsKinematic` in the `RigidBody`
  - Enable `Is Trigger` in the `Box Collider`
  - Use the method `OnTriggerEnter(Collider collider)` to detect collisions between controller and a cube

**c)** The right controller is equipped with a leaser beam, which is used to point at objects in the scene. Per default, this ray is 5 meters long. When the user points with this ray on a cube, the ray ends at the position where it hits the cube. **[2 Points]**  
Implementation hints:
  - Use a `LineRenderer`
  - Start the ray at the position of the `RightControllerAnchor`
  - Use ray-casting in the `Update()` method to detect if a cube is hit

**d)** While pointing at a cube and keeping the A button on the controller pressed the cube slowly rotates around its y-axis. Pressing the B button while pointing at a cube deletes the cube again. **[2 Point]**  
Implementation hints:
  - `OVRInput.Get(OVRInput.Button.One)` returns `true` if the A button is currently pressed
  - `OVRInput.GetDown(OVRInput.Button.Two)` returns `true` if the B button was pressed in the current frame
  - Use `cube.transform.Rotate(new Vector3(0,0.5f,0))` to rotate an `GameObject` called `cube` by 0.5 degrees around its y-axis
  - Use `Destroy(cube)` to delete a `GameObject` called `cube`
