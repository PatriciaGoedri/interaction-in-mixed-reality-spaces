# Interaction in Mixed Reality Spaces: Exercise 2

**Issue Date:** 29.04.2021

**Due Date:**  06.05.2021, 09:00

**Achieved Points**: 	Implementation: **[7/7]**, 	Concept Development: **[6/8]**,	Total: **[13/15]**

## 1. Implementation (7 Points)

In this exercise you will implement an extended version of the often-used Simple Virtual Hand (SVH) technique for all four canonical tasks of 3D object manipulation. Continue with your implementation from the last exercise sheet (i.e. pressing the trigger spawns a cube in front of you).

**a) (De-)Selection.** When touching a cube with the right controller and keeping the button on the side of the controller's grip pressed an object is selected. To provide the user with feedback of this selection, the outline of the object is colored in orange and the controller shortly vibrates on selection. When the button is released, the object is deselected, which is indicated by another short vibration and the disappearance of the orange outline.   **[1 Point]**  
Implementation hints:

- Create the script `SimpleVirtualHand.cs` and add it to the `RightHandAnchor`  in the `OVRCameraRig`
- To get the state of the button on the side of the right controller's grip use `OVRInput.Axis1D.SecondaryHandTrigger`
- Reuse the colliders and triggers of the last exercise sheet
- Detect the currently touched object with `OnTriggerEnter(Collider collider)` and `OnTriggerExit(Collider collider)`
- For the colored outline you can use https://assetstore.unity.com/packages/tools/particles-effects/quick-outline-115488

**b) Positioning & Rotation.** A grabbed (i.e., selected) object is moved and rotated with the controller as if  as if you were holding it in your hand.  **[2 Points]**  
Implementation hints:

- Use `Transform.SetParent()`

**d) Scaling.** With the object selected with the right controller, the left controller can be used to scale the selected object. Therefore, also the button on the side of the left controller's grip needs to be pressed. If the distance between the controllers is increased, the selected object is scaled up uniformly. If the distance between the controllers is decreased, the object is scaled down uniformly.  **[3 Points]**  
Implementation hints:

- To get the state of the button on the side of the left controller's grip use `OVRInput.Axis1D.PrimaryHandTrigger`
- Use `OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch)` to get the position of the left controller
- Use `OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)` to get the position of the right controller
- Every frame calculate how the distance between the controllers changed compared to the last frame, then increase the scale of the grabbed (i.e., selected) object uniformly by this distance change.

## 2. Concept Development (8 Points)

In this part of the exercise you will start to work on a design concept for the topic that you selected in exercise&nbsp;1. You already used the main 3d&nbsp;object for your topic in the implementation part of this exercise. In the concept development part you are supposed to consider how different interaction techniques for 3d&nbsp;manipulation can be used to create a great experience for your topic. You should consider different techniques to support each of the four basic tasks: selection, positioning, rotation, and scaling.

You may draw from the body of interaction techniques for 3d&nbsp;manipulation that we introduced to you as part of the lecture. You may also look into the literature about interaction techniques, or come up with your own ideas.

### Some things that are important:  
+ You should explore different alternatives and consider how well they could support the different basic tasks in your design concept.
+ You should have a clear idea why you selected an interaction technique for your design concept. You should be able to argue for your choice!
+ You should sketch your ideas and document them below (take a look on the lecture slides for an overview on sketching techniques). For the sketching you can make use of the tools and sketching templates that we introduced in session 2. For example: https://medium.com/inborn-experience/templates-for-ar-vr-sketches-e424dfb60e54
+ An easy way to create nice looking sketches without to much effort are hybrid sketches. You can use  images (photos, free images from the web, screenshots taken in the unity editor, etc) and overlay these with other elements (e.g., a hand holding a controller and some lines or arrows that indicate a  movement). You may use this to illustrate the different interaction techniques used in your concept.  
+ Your sketches do not need to be polished, but they need to communicate your ideas!
+ Keep in mind that you work towards one design concept that you should extend in the upcoming exercises and eventually implement with unity&nbsp;3d. That means the interaction techniques you choose should be compatible with one another.
+ You are allowed to extend your design concept by use of additional content like 3d&nbsp;objects, texts, sounds, etc.

### Your tasks:

<ol type="a">
  <li>Create at least <b>eight</b> sketches that show how the different basic tasks can be supported by adequate interaction techniques in your design concept (at least <b>two</b> sketches for the support of each basic task). Make sure that the sketches are understandable and clearly communicate your ideas. <b>[2&nbsp;Points]</b> </li>
  <br>
  <i>[2/2] Nice sketches. A bit confusing that you provide a textual description for only some of them though. Why is that? The sketches are mostly self-explaining, but a bit more information would have been helpful.</i>
  <br>
  <br>
  <li>You already implemented interaction techniques to support each of the four basic tasks in the implementation part of this exercise. You can incorporate the implemented interaction techniques as part of your concept sketches if you like. However, you need to sketch at least <b>four</b> ideas for how <b>different</b> interaction techniques for 3d manipulation can be used in your concept. You do not necessarily need to provide one alternative technique for each  basic task. You can also sketch multiple alternative ideas for techniques that support the same basic task. It is more important that your ideas make sense. For example, you may introduce two alternative ideas for selection and two for positioning if alternative interaction techniques make the most sense for these basic tasks in your design concept. <i>Please note:</i> This task is intertwined with task a. You will receive one point for each idea that clearly communicates the use of an interaction technique for 3d&nbsp;manipulation that differes from the techniques that we introduced in the implementation part of this exercise. <b>[4&nbsp;Points]</b> </li>
  <br>
  <i>[4/4] Nice. </i>
  <br>
  <br>
  <li>Provide a brief argumentation why you selected the technique(s) that your sketches show for each of the four basic tasks in your design concept (consider the parameters  for the basic tasks in the book <i>3d&nbsp;user interfaces, second edition</i>). <i>Please note:</i> This task is intertwined with the tasks a and b. You will receive a half point for a clear argumentation for your choice of interaction techniques related to each of the four basic tasks  (e.g., a half point if you can clearly argument your choice for the interaction techniques that support the basic task <i>selection</i> in your concept). <b>[2&nbsp;Points]</b> </li>
  <br>
  <i>[0/2] Argumentations missing. Why did you chose the techniques that your sketches show?</i>
</ol>

------

*Documentation area (for your documentation!)*

Sketches are provided in the folder named "Sketches".

For the realistic feeling each interaction in the concept takes use of a simple virtual hand (basic tasks before (e.g. character choose) could be represented with a raycast pointer).

1. The first sketch (positioning3-b) shows how an object in the kitchen (e.g. food element) can be selected/grabbed and transported to the trash. After releasing the trigger, the element will be removed/positioned inside the thrash.

2. The second sketch (selection3-b) shows a selection task inside the fridge. Here, the user needs to select a specific element. Visual Feedback is provided while selecting a specific element

3. The third sketch (positiong2-b) shows a positioning task for cutting an object. While pushing the trigger and moving the knife an object can be cutted (if the collide with each other).

4. The fourth sketch (rotate1-b) shows a regulation of the cooktop. While pressing the triggers with the controller, the button of the oven can be rotated to a specific degree to turn on the cooktop.




------
