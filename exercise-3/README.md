# Interaction in Mixed Reality Spaces: Exercise 3

**Issue Date:** 06.05.2021

**Due Date:** 20.05.2021, 09:00

**Achieved Points**: 	Implementation: **[7/7]**, 	Concept Development: **[8/8]**,	Total: **[15/15]**

## 1. Implementation (7 Points)

In this exercise you will implement some locomotion ("how people move through virtual worlds") techniques, wayfinding ("how people orient themselves in virtual worlds and navigate from place to place") aids and an AI agent that always follows you. Again, continue with your implementation from the last exercise sheet.

Preparation: Add the little_maze.fbx from our NavMesh tutorial to your scene.

**a) Teleportation & Snap Turns:** By pressing forward on the left thumbstick a curved ray appears that is always connected to the floor of the little maze. When releasing the stick again the user is teleported to the selected position. By pressing left/right on the thumbstick, the user is turned by  45° degrees to the left/right. **[2 Points]**  
Implementation hints:

- Add a `MeshCollider` to the maze.
- Generate a NavMesh for the maze (as we did it in our NavMesh tutorial).
- You are allowed to reuse scripts/prefabs from the Oculus Integration package, especially have a look at the `PlayerController` prefab:
  - Remove the `OVRCameraRig`
  - Add the `PlayerController` prefab to the scene
  - In the `PlayerController` navigate to the `LocomotionController`
  - Set `AimCollisionLayerMask` of its `TeleportTargetHandlerPhyisical` to `Default`
  - Set `AimButton` and `TeleportButton` of its `TeleportInputHandlerTouch` to `L Index Trigger` only
  - Set `InputMode` of its `TeleportInputHandlerTouch` to `Separate Buttons for Aim And Teleport`

**b) Wayfinding aids:** A line/points/footsteps (freely choose one) on the floor always visualize(s) the shortest path to the top of the bridge to the user. **[3 Points]**  
Implementation hints:

- Use `path = new NavMeshPath()` together with `NavMesh.CalculatePath(Vector3 sourcePosition, Vector3 targetPosition, int areaMask, AI.NavMeshPath path)` and `path.corners`.

**c) AI Agent:** An NPC is always following the user. When the user stops the agent stops 1 Meter away from him. **[2 + 1 Points]**  
Implementation hints:

- You can use a simple capsule for the agent. If you use an animated character instead you will get 1 bonus point.
- Use `NavMeshAgent.SetDestination()` in an `Update()` method to send the agent to your destination every frame.



## 2. Concept Development (8 Points)

In this part of the exercise you will continue to work on your design concept. In the concept development part you are supposed to consider how different travel techniques and wayfinding cues can be used to create a great experience for your topic.

You may draw from the body of travel techniques and wayfinding cues that we introduced to you as part of the lecture. You may also look into the literature about travel techniques and wayfinding cues, or come up with your own ideas.

### Some things that are important:  
+ You should explore different alternatives and consider how well they could support travel and wayfinding in your design concept.
+ You should have a clear idea why you selected a travel technique or wayfinding cue for your design concept. You should be able to argue for your choice!
+ You should sketch your ideas and document them below (take a look on the lecture slides for an overview on sketching techniques). For the sketching you can make use of the tools and sketching templates that we introduced in session 2. For example: https://medium.com/inborn-experience/templates-for-ar-vr-sketches-e424dfb60e54
+ An easy way to create nice looking sketches without to much effort are hybrid sketches. You can use  images (photos, free images from the web, screenshots taken in the unity editor, etc) and overlay these with other elements (e.g., a hand holding a controller and some lines or arrows that indicate a  movement). You may use this to illustrate the different interaction techniques used in your concept.  
+ Your should align your sketches in form of a first narrative storyboard (take a look on the lecture slides for an overview on how to create a narrative storyboard). It is more important that your sketches clearly communicate your ideas than that they are polished!
+ Keep in mind that you work towards one design concept that you should extend further  in the upcoming exercise and eventually implement with unity&nbsp;3d. That means the travel techniques and wayfinding cues you choose should be compatible with one another and with your manipulation techniques from the previous exercise.
+ You are allowed to extend your design concept by use of additional content like 3d&nbsp;objects, texts, sounds, etc.

### Your tasks:

<ol type="a">
  <li>Create a narrative storyboard that shows how the basic tasks for 3d manipulation, travel, and wayfinding are supported in your design concept. Reflect a clearly understandable interaction sequence in your storyboard. In this task you need to think about the interconnection between the different interactions a user can do and also incorporate the context that sorrounds the user. As part of this create at least <b>four</b> sketches that show how travel and wayfinding can be supported by adequate interaction techniques and cues in your design concept (at least <b>two</b> sketches for the support of travel and <b>two</b> sketches for the support of wayfinding). Make sure that the sketches are understandable and clearly communicate your ideas. Use colored annotations to emphasize important user interactions and the use of key interaction techniques in your narrative storyboard. <b>[5&nbsp;Points]</b> </li>
 <br>
<i>[5/5] Nice sketches. Just two suggestions for future documentations that may help to facilitate understanding: (i) You can directly embed images in the documentation (makes it easier readable). (ii) You may consider it to provide short textual descriptions for all parts of a graphic that you include (e.g., the different frames in the storyboard).</i>
  <br>
  <br>
  <li>You already implemented a travel technique and a wayfinding cue in the implementation part of this exercise. You can incorporate the implemented travel technique and wayfinding cue as part of your narrative storyboard. However, you need to sketch at least <b>two</b> ideas for how <b>different</b> travel techniques or wayfinding cues can be used in your concept. You do not necessarily need to provide one alternative technique for travel and one alternative cue for wayfinding. You can also sketch two alternative ideas for travel or wayfinding. It is more important that your ideas make sense.  <i>Please note:</i> This task is intertwined with task a. You will receive one point for each idea that clearly communicates the use of a travel technique or wayfinding cue that differes from the technique and cue that we introduced in the implementation part of this exercise. <b>[2&nbsp;Points]</b> </li>
  <br>
<i>[2/2] Nice. As mentioned in the last session, also think about the details. For instance, how will the user target parts of the environment that s/he wants to travel to? How is travel initiated? What feedback is provided to the user? How do you make sure that this works well in combination with the natural walking technique? (e.g., if natural walking is used in rooms and to travel between rooms, you do not want to have a user travel into a different room by teleport facing a wall in the real world and an open room space in the virtual environment, as this would make natural walking impossible…) </i>
  <br>
  <br> 
  <li>Provide a brief argumentation why you selected the travel technnique(s) and wayfindings cue(s) that your narrative storyboard shows. <i>Please note:</i> This task is intertwined with the tasks a and b. You will receive a half point for a clear argumentation for your choice of travel technique(s) and a half point for a clear argumentation for your choice of wayfinding cue(s). <b>[1&nbsp;Point]</b> </li>
  <br>
  <i>[1/1] Nice. Just as a suggestion for your future documentation: You may consider also mentioning your decision paths. Which options did you consider and why did you settle with the options that you documented? (for example, why did you finally go for A and not B? This might also be helpful when you report your work progress in the implementation part as it can support you in clarifying your decision making process.)</i>
</ol>

------

*Documentation area (for your documentation!)*


a) The storyboard (./Sketches/storyboard_imrs.PNG) shows two travel techniques. First the simple walking.This allows a more realistic feeling, as close locations in space can be easily reached. Another technique is simple teleportation. Here, one of the 2 possible doors is selected (if they are visible) to change the room in the shortest time. This is possible because the Cooking Adventure is a small environment. One wayfinding cue, is the highlighting of destination elements. Therefore, the user can quickly learn where his next destination is supposed to be. Another wayfinding cue are prints on the floor, whihc indicate the next destination step in the floor. Here, the user is only navigated to the position and must search for the next task.


![Storyboard](./patricia_goedri/ecercise-3/Sketches/storyboard_imrs.PNG)



b) ![B1](./patricia_goedri/ecercise-3/Sketches/b1.PNG)

The sketch above shows another different wayfinding cues, which could be inserted into the Cooking Adventure. Here, the usage of spatial sound helps the user to find his last room to serve the meal.
![B2](./patricia_goedri/ecercise-3/Sketches/b2.PNG)
Another possible travel technique is the world-in-miniature. Here, the user can easily switch between the room by selecting the destination on the world-in-miniature map-
------
