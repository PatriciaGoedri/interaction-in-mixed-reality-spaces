using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFinder : MonoBehaviour
{

    public NavMeshPath path;
    public LineRenderer line;
    private float elapsed = 0.0f;
    public CapsuleCollider CharacterController;



    void Start()
    {
        //player = GetComponent<PlayerController>();
        line = GetComponent<LineRenderer>();
        //path = new NavMeshPath();
        elapsed = 0.0f;

        line.startWidth = 0.15f;
        line.endWidth = 0.15f;

        line.positionCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        path = new NavMeshPath();
        
        Vector3 destination = new Vector3(8,8,8);

/*
        Debug.Log("dest pos");
        Debug.Log(destination);
        Debug.Log("player pos:");
        Debug.Log(transform.position);
        Vector3 allWayPoints = new Vector3[path.corners.Length];
        Debug.Log("waypoints");
        Debug.Log(allWayPoints);

        Debug.Log("path:");

        Debug.Log(NavMesh.CalculatePath(CharacterController.transform.position, destination, NavMesh.AllAreas, path));
        
        //line.positionCount = path.corners.Length;
        //line.SetPosition(0, destination);
    */
        
       elapsed += Time.deltaTime;
        if (elapsed > 1.0f)
        {
            elapsed -= 1.0f;
            NavMesh.CalculatePath(CharacterController.transform.position, destination, NavMesh.AllAreas, path);

            //Debug.Log("Path waypoints");
            //Debug.Log(NavMesh.CalculatePath(CharacterController.transform.position, destination, NavMesh.AllAreas, path));
        }
        for (int i = 0; i < path.corners.Length - 1; i++)
        {
            line.SetPositions(path.corners);
        }
            
        
    }


}

