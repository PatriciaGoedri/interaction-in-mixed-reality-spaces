﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCollorOnCollision : MonoBehaviour
{



    void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.name == "RightHandAnchor")
        {
            GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            OVRInput.SetControllerVibration(0.1f, 0.2f, OVRInput.Controller.RTouch);
            StartCoroutine(VibrateRightAfterTime(0.2f));
            
        }

        if (collider.gameObject.name == "LeftHandAnchor")
        {
            GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            StartCoroutine(VibrateLeftAfterTime(0.2f));
        }
    }

    IEnumerator VibrateLeftAfterTime(float time)
    {
        OVRInput.SetControllerVibration(0.1f, 0.2f, OVRInput.Controller.LTouch);
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
    }

    IEnumerator VibrateRightAfterTime(float time)
    {
        OVRInput.SetControllerVibration(0.1f, 0.2f, OVRInput.Controller.RTouch);
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
    }
}
