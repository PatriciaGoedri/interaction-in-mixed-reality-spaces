using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerNavigation : MonoBehaviour
{
    // Start is called before the first frame update

    public NavMeshAgent agent;
    public OVRCameraRig CameraRig;
    public CapsuleCollider CharacterController;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(CharacterController.transform.position);
    }
}
