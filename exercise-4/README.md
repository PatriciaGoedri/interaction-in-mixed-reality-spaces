# Interaction in Mixed Reality Spaces: Exercise 4

**Issue Date:** 20.05.2021

**Due Date:**  27.05.2021, 09:00

**Achieved Points**: 	Implementation: **[7/7]**, 	Concept Development: **[6/8]**,	Presentation: **[22/25]**,	Total: **[35/40]**

## 1. Implementation (7 Points)

In this part of the exercise you will implement a menu for the your implementation of the exercises and your project. Before you start, have a look at the sample scenes `OVROverlay.unity` and `OVROverlayCanvas.unity` in the Oculus Integration package. You find them under `Oculus > SampleFramework > Usage`. Your menu implementation should fulfill the following requirements. You are allowed to reuse scripts form the Oculus Integration Package.

**a) UI Input** via Raycasting: Starting from the controller, a ray is shot into the scene. When the ray hits an UI element (like a canvas), a cursor appears. To interact with UI Elements like sliders, buttons, text fields, ... the user needs to hover with the cursor over them and press the trigger button. **[1 Point]**
Implementation hints:

- You can duplicate and adapt the sample scene `OVROverlayCanvas.unity` for this exercise

**b) Title & Scene Selection** with button "Load scene": The menu contains a title for exercises and project (e.g., Interaction in Mixed Reality Spaces '21 & Project Title) and allows to select between four scenes using little preview images for the scenes and a load button. The first three scenes are the exercises 1, 2 and 3. The fourth scene will be the start scene of your project. The user can select a scene via raycasting. When he presses the load button, the selected scene is loaded.  **[2 Points]**
Implementation hints:

- Use toggles in a ToggleGroup for the scenes (e.g. you could remove the label and the checkmark, set a "Selected Color" and use a preview image of the scene as background)
- Make sure, that only one scene can be selected
- Use `SceneManager.LoadScene("Scene Name", LoadSceneMode.Single)` to load a different scene
- Make sure that you add all scenes to "Scenes in Build" in the build manager
- Make sure that you menu scene is the first scene ("0") in the build manager

**c) Text field for player name:** The menu allows the user to set a player name. When the user clicks on this input field a keyboard opens up, where he can type in his name via raycasting. **[2 Points]**
Implementation hints:

- Follow this guide to open the keyboard of the Oculus Quest 2: https://developer.oculus.com/documentation/unity/unity-keyboard-overlay/
- The keyboard only works when you build the app on the device

**d) Slider** for adjusting the volume: The menu allows the user to adjust the volume of the game via a slider. **[1 Point]**
Implementation hints:

- It is enough if the slider works, (i.e., the user can interact with it); you don't need to adjust the devices volume based on the slider's value

**e) Radio Buttons** for adjusting the difficulty level: The menu allows the user to chose between three difficulty levels. **[1 Point]**

- Make sure that only one difficulty level can be selected
- Again, it is sufficient if the interaction with the radio buttons works; the setting of a different difficulty level does not need to have an effect on your scenes

**Bonus:** We reward one bonus point for good design and usability of the menu (cf. http://www.csun.edu/science/courses/671/bibliography/preece.html and https://www.nngroup.com/articles/ten-usability-heuristics/). **[1 Bonus Point]**

## 2. Concept Development (8 Points)

In this part of the exercise you will finalize your design concept. You are supposed to consider how different interaction techniques for system control and symbolic input can be used to create a great experience for your topic and to refine your storyboard so that it reflects a great and holistic concept for your project.

For the concept refinement, you may draw from the body of interaction techniques for system control and symbolic input that we introduced to you as part of the lecture. You may also look into the literature about interaction techniques for system control and symbolic input, or come up with your own ideas.

### Some things that are important:  
+ You should explore different alternatives and consider how well they could support system control and symbolic input in your design concept.
+ You should have a clear idea why you selected the interaction techniques and cue(s) in your final design concept. You should be able to argue for your choices!
+ You should sketch your ideas and document them below. For the sketching you can make use of the following sketching templates: https://medium.com/inborn-experience/templates-for-ar-vr-sketches-e424dfb60e54
+ An easy way to create nice looking sketches without to much effort are hybrid sketches. You can use  images (photos, free images from the web, screenshots taken in the unity editor, etc) and overlay these with other elements (e.g., a hand holding a controller and some lines or arrows that indicate a  movement). You may use this to illustrate the different interaction techniques used in your concept.  
+ Your should align your sketches in form of a polished narrative storyboard. It is important that your sketches clearly communicate your ideas, but for this exercise they should also look nice as you will use them for your concept presentation!
+ Keep in mind that you should have one holistic design concept after you completed this exercise, that you will implement with unity&nbsp;3d in the upcoming weeks.  That means the different interaction techniques and cues you use in your concept should be compatible with one another (this includes techniques for 3d manipulation, travel techniques, wayfinding cues, and techniques for system control and symbolic input).
+ You are allowed to extend your design concept by use of additional content like 3d&nbsp;objects, texts, sounds, etc.

### Your tasks:

<ol type="a">
  <li>Refine your narrative storyboard.  Make sure that your narrative storyboard shows how the basic tasks for 3d manipulation, travel, wayfinding, system control, and symbolic input are supported in your design concept. Reflect a clearly understandable interaction sequence in your storyboard. As part of this create at least <b>four</b> sketches that show how system control and symbolic  input can be supported by adequate interaction techniques in your design concept (at least <b>two</b> sketches for the support of system control and <b>two</b> sketches for the support of symbolic input). Make sure that the sketches are understandable and clearly communicate your ideas.  Make the sketches look nice so that you can use them for your concept presentation (see the next part of this exercise). Use colored annotations to emphasize important user interactions and the use of key interaction techniques in your narrative storyboard. Your  final concept needs to include at least <b>four</b> interaction techniques or cues that you did not implement as part of the exercises 1-4 in Unity 3d, yet. <b>[4&nbsp;Points]</b> </li>
<br>
<i>[4/4] Very nice final storyboard. Additional short textual descriptions would have been helpful.</i>
<br>
<br>
  <li>Provide a brief argumentation why you selected the (i) interaction technniques for the four basic tasks selection, positioning, rotation, and scaling, (ii) interaction technniques for navigation and cues for wayfinding, and (iii) interaction techniques for system control and symbolic input, that your narrative storyboard shows. <i>Please note:</i> This task is intertwined with the tasks a. You will receive a half point for a clear argumentation for your choices for each type of interaction technique - e.g., a half point for a clear argumentation for your choice of selection technique(s) - and for a clear argumentation for your choice(s) of wayfindings cue(s). <b>[4&nbsp;Points]</b> </li>
<br>
<i>[2/4] Overall, it could have been made more clear why you choose the techniques that your storyboard shows in contrast to different options (you provided only few information about your decision making process). For selection, position, rotation, and scaling also a clear naming of your interaction technique choices is missing (you provide rather vague descriptions in these sections).</i>
</ol>
<br>



###  a. Your Final Design Concept in Form of a Narrative Storyboard

------


<img src="Sketches/storyboard.png" alt="storyboard" width="550"/>
------

###  b. Your Argumentations for your Choices for each Type of Interaction Technique and Wayfinding Cues
*For example: Navigation is supported in my final concept by [some technique, e.g.,  a world-in-minature] and [potential other technique(s), if applicable], because [your reason].*

------

+ **Selection Technique(s):** Selection is supported in my final concept by gathering the ingredients and work tools (e.g. knife).

+ **Positioning Technique(s):** Positioning is supported in my final concept by grabbing and transporting the ingridients, as well as the tools for process them.

+ **Rotation Technique(s):** Rotation is supported in my final concept by regulating the button for the hot plate.

+ **Scaling Technique(s):** Scaling is supported in my final concept by using the controllers to larger the bread for further processing, because

+ **Navigation Technique(s):** Navigation is supported in my final concept by simple walking and teleporting with the magic spoon. Therefore nearby areas can be reached by simply walking and distant places (such as other rooms) can be reached by teleporting.

+ **Wayfinding Cue(s):** Wayfinding Cues are supported in my final concept by spatial sound, highlighted areas with particles surrounding them as well as pathfinding to a broader area. This helps the user with different levels of support.

+ **Symbolic Input Technique(s):** Symbolic Input technique is supported in my final concept by a virtual keyboard, to allow the user to input player settings.

+ **System Control Technique(s):** System Control Techniques are supported in my final concept by a 1-DOF Menu for adjusting player setting, as well as a always available task overview. Furthermore 1-DOF dialog menus are implemented to inform the player about new interaction tools.

------

## 3. Presentation (25 Points)

The presentation  of your final concept will be done in form of a video presentation. You will need to conduct a video recording of your presentation and upload it to ILIAS until the deadline on the due date (27.05.2021, 09:00).

### Some things that are important:  
+ The total time for presentation is maximum **six** minutes per person! Each presenter will be strictly held to the time limit. If you go over, your grade will be affected.
+ Your presentation should look professional, prepared with presentation software (e.g.,  PowerPoint).
+ Make all information on the slides well perceivable (avoid tiny images that no one can understand and small text sizes that make text unreadable).
+ Number your slides.
+ Avoid it to speak to fast and speak clearly.
+ Avoid overloaded slides and reduce the amount of text to the minimum necessary to convey the information that is essential.
+ You can find the grading sheet for the presentation for download in ILIAS.

### Presentation Structure (Your tasks):

The focus of the presentation is your final design concept. Your presentation should address the following points:

*a. Introduction  [approximately 1 minute]*
+ Title slide (your name, topic, matricle number, etc).
+ Briefly introduce the topic for which you present a design concept.
+ Give a brief overview on the interaction techniques and cues that your design concept makes use of (techniques for 3d manipulation, travel, system control, symbolic input, and wayfinding cues).

*b. Narrative Storyboard [approximately 4 minutes]*
+ Present your narrative storyboard as one clearly understandable interaction sequence. If your whole storyboard is to long to show it in the presentation, focus on the most important parts  and leave less important parts out.
+ Highlight which aspects of interaction your concept includes and when they are reflected in the narrative storyboard (e.g., by use of annotations / colors). This concerns:  
  - Interaction techniques for 3d manipulation.
  - Travel techniques and wayfinding cues.
  - Interaction techniques for system control and symbolic input.
+ Explain each step of the storyboard clear, vivid, and in sufficient detail so that the audience can understand it.
+ Additionally, you may provide arguments for your choices of interaction techniques and cues where this is appropriate.

*c. Outlook [approximately 1 minute]*
+ Provide a simple gantt chart that shows how you plan to implement your design concept in unity 3d in the upcoming weeks (as part of exercise 5 and 6). You can use the following template as a basis: <a href=https://templates.office.com/en-us/two-month-gantt-chart-tm56247502?omkt=en-001>https://templates.office.com/en-us/two-month-gantt-chart-tm56247502?omkt=en-001</a>
+ You need to implement at least <b>two</b> interaction techniques and cues from your design concept - that were not implemented as part of the exercises 1-4! - until the <i>Presentation of the intermediate app state</i> (part of exercise 5, submission deadline: 24.06.2021, 09:00). Make sure to consider this when creating the gantt chart.
+ You may  add a time buffer (e.g., 3-4 days) in case of unexpected delays.
+ You may highlight important milestones (e.g., the completion of a feature).

The structure above provides a suggestion that you can use as a template for the structuring of your own presentation. You may have to adapt it to live within the six-minute limit.  You must practice your presentation several times to get a smooth and interesting talk and keep within your time limit.

### Presentation Submission

*a. PDF of presentation*
+ Submit your presentation slides in ILIAS in form of a single pdf file into the folder: <a href=https://ilias.uni-konstanz.de/ilias/goto.php?target=fold_1245997>https://ilias.uni-konstanz.de/ilias/goto.php?target=fold_1245997</a> (<i>I. Presentation of Final Design Concept/Presentations (Student Submissions)</i>)
+ Name the file (in this order, please): “&ltcourse name&gt - &ltcurrent semester, year&gt - final design concept - &ltyour name&gt.pdf”.  If you do not follow the naming schema provided, your grade will be affected negatively.

*b. MP4 Video of presentation*
+ Submit your video in ILIAS in form of a single mp4 file into the folder: <a href=https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1245998.html>https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1245998.html</a> (<i>I. Presentation of Final Design Concept/Videos (Student Submissions)</i>).
+ Name the file (in this order, please): “&ltcourse name&gt - &ltcurrent semester, year&gt - final design concept - &ltyour name&gt.mp4”.  If you do not follow the naming schema provided, your grade will be affected negatively.
+ You can directly upload videos <= 256 MB to ILIAS. If this should not work, you may upload your video to the nextcloud of the university (<a href=https://cloud.uni-konstanz.de>https://cloud.uni-konstanz.de</a>) and add a link to your video in ILIAS.
+ For the creation of a remote presentation you may have a look at the following guide (you do not need subtitles!): <a href=https://medium.com/sigchi/a-remote-video-presentation-guide-93957c63aa7a>https://medium.com/sigchi/a-remote-video-presentation-guide-93957c63aa7a</a>
+ We recommend you the use of a resolution of 1920x1080.
+ To include a video of the presenter on your slides is optional (you are free to do so, but it is not a must have). Powerpoint (Office 365) supports this out of the box now: <a href=https://support.microsoft.com/en-us/office/record-a-slide-show-with-narration-and-slide-timings-0b9502c6-5f6c-40ae-b1e7-e47d8741161c#OfficeVersion=Microsoft_365_for_Windows>https://support.microsoft.com/en-us/office/record-a-slide-show-with-narration-and-slide-timings-0b9502c6-5f6c-40ae-b1e7-e47d8741161c#OfficeVersion=Microsoft_365_for_Windows</a>.
+ To decrease the size of your video before its upload you can use handbrake: <a href=https://handbrake.fr>https://handbrake.fr</a>.
