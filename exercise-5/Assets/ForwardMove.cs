using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardMove : MonoBehaviour
{

    OVRInput.Controller activeController =  OVRInput.GetActiveController();
    public Transform trackingSpace;
    public GameObject cameraRig;

    public GameObject rightController;

    public GameObject rightControllerAnchor;
    public GameObject ObjectPull;
    public float velocity;
    //public GameObject rightHand;

    void Start()
    {
         Vector3 leftPosOld = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        Vector3 rightPosOld = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
    }
    void Update()
    {

        rightControllerAnchor.transform.SetParent(this.transform);
        Vector3 positionRight = trackingSpace.TransformPoint(OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch));

        //Debug.Log(position);

        Vector3 leftPosOld = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        Vector3 rightPosOld = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);


        Vector3 meshPosR = rightControllerAnchor.transform.position;

        float scale = 0.001f * meshPosR.x  ;


        //rightControllerAnchor.transform.position += rightControllerAnchor.transform.forward * scale ;

       // rightControllerAnchor.transform.position += scale * cameraRig.transform.forward; //(works with moving head)

       rightControllerAnchor.transform.position += -scale * rightController.transform.forward;



/*
        Debug.Log("acivated:");
        Debug.Log(rightPosOld);
        

        Debug.Log("Mesh pos:");
        Debug.Log(meshPosR);


*/
        if (activeController == OVRInput.Controller.RTouch)

        {

           // Debug.Log("Position Old:" + rightPosOld);
            

            Vector3 leftPosNew = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
            Vector3 rightPosNew = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
           // Debug.Log("Position New:" + rightPosNew);

            float distanceRight = Vector3.Distance(rightPosOld, rightPosNew);
  

        }
        
        
        //rightControllerAnchor.transform.position += posRTouch;
        
    }


    public void PullObject()
    {

    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if(collisionInfo.gameObject.CompareTag("Pull"))
        {
            ObjectPull = collisionInfo.gameObject;

            ObjectPull.transform.position = Vector3.MoveTowards(ObjectPull.transform.position, cameraRig.transform.position, velocity * Time.deltaTime);
        }
    }
}
