using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PullObject : MonoBehaviour
{
    public GameObject ObjectPull;
    public float velocity;

    bool m_inRange;

    public GameObject cameraRig;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == ("Pull")){
            ObjectPull = other.gameObject;

            ObjectPull.transform.position = Vector3.MoveTowards(ObjectPull.transform.position, cameraRig.transform.position, velocity * Time.deltaTime);
        }
    }
   


}
