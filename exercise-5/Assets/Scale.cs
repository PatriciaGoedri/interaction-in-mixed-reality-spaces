using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{

    public GameObject selectedObject;

    public GameObject collision;
  
    // Update is called once per frame
    void Update()
    {
        if(OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger, OVRInput.Controller.Touch) > 0.3f && collision)
        {
            SelectCube();        
        }

        if(OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger, OVRInput.Controller.Touch) < 0.3f && selectedObject)
        {
            ReleaseCube();
        }

        
    }

    public void SelectCube()
    {
    
    
        selectedObject = collision;
        selectedObject.transform.SetParent(this.transform);
        

       selectedObject.GetComponent<Rigidbody>().isKinematic = true;
       //selectedObject.GetComponent<Outline>().enabled = true;

        //scale cube while selected
        if(OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.Touch) > 0.3f && selectedObject)
            {
                ScaleCube();
            }
    }


    public void ScaleCube()
    {
        Vector3 leftPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        Vector3 rightPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

        float distance = Vector3.Distance(leftPos, rightPos);

        //this.transform.localScale = Vector3.one * distance;
        selectedObject.transform.localScale = Vector3.one * distance;
    }


    public void ReleaseCube()
    {
        selectedObject.GetComponent<Rigidbody>().isKinematic = false;
        selectedObject.transform.SetParent(null);
        selectedObject = null;   
            
    }

    void OnTriggerExit(Collider collider)
    {
        collision = null;
        OVRInput.SetControllerVibration(0.1f, 0.2f, OVRInput.Controller.RTouch);

        collider.GetComponent<Outline>().enabled = false;
    }

    void OnTriggerEnter(Collider collider)
    {     
        collision = collider.gameObject; 
        collider.GetComponent<Outline>().enabled = true;     
    }


}
