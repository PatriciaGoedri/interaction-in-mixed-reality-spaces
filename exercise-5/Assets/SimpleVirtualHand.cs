using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleVirtualHand : MonoBehaviour
{

    GameObject touchedObject;

    GameObject grabbedObject;

    float distanceLastFrame;

    bool scaling = false;

    void OnTriggerEnter(Collider collider)
    {
        if (!grabbedObject)
        {
            touchedObject = collider.gameObject;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        touchedObject = null;
    }

    // Update is called once per frame
    void Update()
    {
        // Check if right trigger button was pressed this frame 
        if (OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.8f && touchedObject)
        {
            if (!grabbedObject)
            {
                StartCoroutine(VibrateRight(0.1f));
            }
            grabbedObject = touchedObject;
            grabbedObject.transform.SetParent(this.transform);
            grabbedObject.GetComponent<Outline>().enabled = true;
        }

        if (OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) < 0.2f && grabbedObject)
        {
            grabbedObject.transform.SetParent(null);
            grabbedObject.GetComponent<Outline>().enabled = false;
            scaling = false;
            grabbedObject = null;
        }

        if (OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.8f && OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0.8f && grabbedObject)
        {
            Vector3 posLTouch = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
            Vector3 posRTouch = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
            float distance = Vector3.Distance(posLTouch, posRTouch);

            if (!scaling)
            {
                scaling = true;
            }
            else
            {
                float distanceChange = distance - distanceLastFrame;
                grabbedObject.transform.localScale += new Vector3(distanceChange, distanceChange, distanceChange);
            }

            distanceLastFrame = distance;

        }

        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) < 0.2f)
        {
            scaling = false;
        }
    }

    IEnumerator VibrateRight(float time)
    {
        // Start vibration
        OVRInput.SetControllerVibration(0.1f, 0.2f, OVRInput.Controller.RTouch);
        yield return new WaitForSeconds(time);

        // Stop vibration
        OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
    }

}
