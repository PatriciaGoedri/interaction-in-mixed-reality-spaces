# Interaction in Mixed Reality Spaces: Exercise 5

**Issue Date:** 27.05.2021 

**Due Date:**  24.06.2021, 09:00

**Achieved Points**: 	Theory: **[17.5/20]**, 	Project: **[14/20]**,	Presentation: **[20/20]**,	Total: **[51.5/60]**

## 1. Theory (17.5 / 20 Points)

*a.* Explain Mixed Reality as defined by Milgram and Kishino’s Virtuality Continuum. Describe one example for Augmented Reality (AR), and one example for Augmented Virtuality (AV). **[1.5/1.5&nbsp;Points]**

------

*Mixed Reality is defined as the space between only physical environment and a fully virtual environment. An Exaple for AR is the furniture app IKEA Place. This allows users to place virtual furniture in a real or own room. An Example for Augmented Virtuality is an application that allows to try on clothes, where the user is integrated into the virtual world/cabin*

+ *In case you should consider it to refer to the continuum in the future, please note: The continuum of Milgram and Kishino focuses on Mixed Reality **Visual Displays**. (I mention this, as this is not part of your answer.)*

------

*b.* Give an example of how the three defining characteristics of Augmented Reality (according to Azuma) are reflected in the AR configuration of the following video (between 0:21–0:33): [https://www.youtube.com/watch?v=DWZ80TfFiFI](https://www.youtube.com/watch?v=DWZ80TfFiFI) **[1.5/1.5&nbsp;Points]**

------

*Combines real and virtual: you can see either the environment as well the virtual blocks
Interactive in real time: Interacting with the memory block reveals the pattern of it
 Registered in 3-D: The whole memory puzzle is built in 3-D integrated in the room.*

------

*c.* You learned about the differences between external and internal tracking in the lecture. Describe one example for an external tracking approach and one example for an internal tracking approach. Describe one advantage and one disadvantage for each. **[2/3&nbsp;Points]**

------

*
External: Usage of HTC Vive Device. Tracking with Lighthouse sensors that are placed around the room. The advantage is a very good accuracy since the trackers are fixed (increasing accuracy with more trackers). One disadvantage is occlusion. If the sensors aren't tracking the objects in 360 degrees, the tracking could get lost in case of occlusion of other objects.

Internal: Using Markers as anchors points in vision-based tracking (e.g. QR-Codes). The advantage is flexibility in movement. One disadvantage is that the markers may not be detectable under severe lighting conditions.

* *Where are the markers? On the device? In the environment? Where is the camera? I guess that you refer to an augmentation of the physical environment with QR-Codes. If you meant markers that are placed around a city and that are only used to identify POIs while the tracking is handled by the internal sensors of, e.g., a handheld device, you should make this clear and write it down. Your answer is for most parts not clearly related to internal tracking. Still, it is true, that internal tracking can provide you with more flexibility.*

------

*d.* Briefly describe two possible issues that can occur when voice commands are used for system control. Use real-world example(s) to make your descriptions easily understandable. **[1/1&nbsp;Point]**

------

*wrong activation trigger for input. It is known that Amazons Alexa reacts to more activation triggers which sound similar. If only voice commands are used persons with speech impairments can't interact with the system. *

------

*e.* As you learned in the lecture, it is possible to distinct manipulation tasks in form of basic tasks. Name the four different basic tasks for 3D manipulation and give  a clear, unambiguous example for each. **[2/2&nbsp;Points]**

------

*selection: picking an apple , positioning: moving the apple to a new destination , rotation: turn over the apple , scaling: Not in Real-World but VR: increasing the size of the apple.*

------

*f.* Describe one vector-based pointing technique, and one volume-based pointing technique. Describe one advantage and one disadvantage for each of the two techniques. **[3/3&nbsp;Points]**

------

*A vector-based pointing technique is raycasting. The advantage is a high precision of selection. A disadvantageis th amplification of the jitter of hand and tracker with increasing distance.

A volume-based pointing technique is the flashlight. The advantage is that small objects can be selected easily even when they are located far from the user. A disadvantage is disambiguation of the desired object when more than one object falls into the spotlight.*

------

*g.* The image below shows the YAW Motion Simulator. Users sit in a chair with a VR headset for a more immersive VR experience. Consider the scenario of a racing game, where a user sits in this chair while moving forward in a virtual car in VR: The car moves when a button is pressed on a controller. The chair swivels around itself automatically to simulate forces while the user remains seated in the chair. The user can look around by moving her head.

![yaw motion simulator](https://unboundvr.nl/media/catalog/product/cache/1/thumbnail/600x600/e4d92e6aceaad517e7b5c12e0dc06587/p/r/pro1_b24b1f54-ab36-4244-bba5-fccba0230ed9_1024x1024_2x.jpg)

Consider the classification of travel techniques in active and passive techniques. In which part of the scenario is the travel of the user active and passive? Name one case in which the user’s travel is active and one case in which it is passive and for each describe why it is active/passive. **[1/1&nbsp;Point]**

------

*The travel technique is active while the user is using the controller and presses the button and moving her head physically, because she directly controls the movement. It is inactive when the chair swivels around itself automatically to simulate forces, because the viewpoint movement is controlled by the YAW Motion Simulator*

------

Consider the classification of travel techniques in physical and virtual techniques. In which part of the scenario is the travel of the user physical and virtual? Name one case in which the user’s travel is physical and one case in which it is virtual and for each describe why it is physical/virtual. **[0.5/1&nbsp;Point]**

------

*The travel technique is physical when the user looks around by moving her head, because she takes use of a physical rotation. The travel technique is virtual while the while the user remains seated in the chair even though the simulator changes the virtual viewpoint.*

+ *What does "changes the viewpoint" mean? This could refer to driving, head movement, or swiveling of the chair... An example for a virtual travel techniqe would be that the virtual car moves virtually during driving, but the user's body does not physically move along with the car. If this is what you meant, please be more clear about this next time.*      

------

*h.* In the image below, you can see the go-go interaction technique in action. Briefly describe how the interaction technique works.
State one advantage and one disadvantage of the go-go interaction technique.
Is this interaction technique isomorphic or non-isomorphic? State
the reason why. **[1.5/2&nbsp;Points]**

![go-go interaction technique](https://www.researchgate.net/profile/Mark-Billinghurst/publication/2817497/figure/fig4/AS:669560822829076@1536647148912/The-Go-Go-technique-allows-users-to-expand-their-reach-The-white-cube-shows-the-real.png)

------

*The Go-Go interaction provides an unobtrusive technique that allows the user to interactively change the length of the virtual arm. Therefore, if the user’s real hand is close to the user, Go-Go uses a one-to-one mapping, so that the movements of the virtual hand correspond to the real hand movements. If the user extends the hand beyond a predefined distance threshold, the mapping becomes nonlinear and the virtual arm extends towards the target. An advantage is that it provides a seamless, 6-DOF object manipulation either on close or far objects. A disadvantage is that it complicates precise positioning at far distance.*

+ *Part of the question not answered: "Is this interaction technique isomorphic or non-isomorphic? State the reason why."*

------

*i.* You want to support a user in wayfinding by use of a a small map
which can be displayed on an AR head-mounted display. The map is supposed to help the user find her/his way to certain locations in a city at which s/he can find interactive AR puzzles. Briefly explain one aspect that you, as the application designer should take into account to make sure that the wayfinding support is helpful to the user. **[0.5/0.5&nbsp;Points]**

------

*The wayfinding cues should be always in the field-of-view but not occluding the whole environment, so that the user gets easily supported by the cues while focussing on the environment for preventing accidents.*

------

*j.* Consider the following scenario: You want to create an AR application that is used with a head-mounted AR display like the Microsoft Hololens. The application’s users will interact with a diverse set of objects. Each object type requires a different input method: Some are best suited for text input, others use color input, and so on. Which keyboard-based input technique would you choose in this scenario? Clearly explain your decision. **[1/1&nbsp;Point]**

------

*Best fitting keyboard-based input would be the soft keyboard. Since you need to provide possibilities for alternative input the UI of the soft keyboard can be adjust for each case. E.g. using a color pallete, simple virtual keyboard for text input etc. *

------

*k.* Consider the following scenario: You want to create an AR application that is used with a head-mounted AR display. The purpose of the application is to support the co-located, collaborative analysis of small data visualizations. As part of the applications use, the users will move around in a physical room to view different small, individual visualizations from all sides. The application should also support it that users can discuss possible findings directly in AR. To do so, they should be able to speak naturally, but also to use bodily gestures like pointing. Would you use a video see- through or an optical see-through display in this scenario? State two reasons that clearly explain your decision. **[1/1&nbsp;Points]**

------

*An optical see-through display would work better in this case. First of all both users are aware of their surroundings as well of each other. therefore, they could easily exchange pointing gestures and still focus on the AR content. This would avoid complicated software implementations (e.g. raycasting pointing).*

+ *I do not think, you would need raycast pointing or similar with a video see-through. But, you are right, communication would probably be affected.* 

------

*l.* Name and describe the three different kinds of spatial knowledge that were introduced in the lecture. Give a clear, unambiguous example from your real world surroundings for each kind of spatial knowledge. Your examples have to be different from the examples provided in the lecture. (e.g., Münster in Konstanz = Landmark) **[1/1.5&nbsp;Points]**

------

*Landmark: main building of the University of Konstanz
Procedural knowledge: Traffic plan for bus stops
Survey knowledge: Fountain in front of the main building of the University of Konstanz *

+ *All three kinds of spatial knowledge named and three examples provided. The example for survey knowledge could be more clear. Part of the task not completed: "Name and  **describe** the three different kinds of spatial knowledge that were introduced in the lecture".*

------

## 2. Project (14/20 Points)

In this part of exercise 5 you are supposed to implement the first half of your final design concept with unity 3d and to document your work progress. You are allowed to use everything that you created during the exercises 1-4 as a basis. Please follow the instructions below:

*a. Implementation **[8/10&nbsp;Points]***

+ You need to implement at least **two** interaction techniques/cues from your design concept, which were not implemented as part of the *implementation part* from the exercises 1-4!
+ Submit the code for your implementation to the Gitlab (make sure that your code can be easily executed on a Quest 2 and runs without errors).
+ Also submit an apk for your implementation that can be installed on the Quest 2  to the Gitlab.
+ Document the c# code in your project.

*Ok, but the two interaction techniques are not both "complete" (did not reach the state of completion that was expected for the intermediate presentation) and the current version is not very stable (e.g., easily possible to "fall" out of the room).*

*b. Documentation of Work Progress **[6/10&nbsp;Points]***
+ Document your work progress in the readme file, below.
    - If your app requires a certain setup, make sure to explain how the setup is done (e.g., by placing markers).
    - Document how you implemented different interaction techniques and cues. This concerns:
        - Interaction techniques for 3d manipulation.
        - Travel techniques and wayfinding cues.
        - Interaction techniques for system control and symbolic input.                  
    - Document your intermediate app state in a logical order (If possible, try to tell a story. You may make use of your original storyboard for inspiration!).
    - Provide arguments for important design decisions (why did you decide for option A instead of option B?).
    - Document the problems you ran into and how you solved/addressed them (see the instructions for the creation of your intermediate presentation for details).
+ You can include images, videos, and code fragments to illustrate parts of your documentation.
+ Finally, you are permitted to update your concept as part of your project work if this should be necessary (for example, because of technical limitations). If you make important changes to your concept as part of this exercise, please briefly clarify how and why.

------
First of all I've started setting up the primary environment with the kitchen asset. For each room I have created a unique scence. After that I've adapted the canonical interaction techniques which were implemented in the exercises before, for the simple interaction (Scale.cs). While Testing it occured that the original kitchen asset provided problems/failures with "Self-interactable models". Also it wasn't possible to place objects on the surface. Therefore, I found a new (better) asset, where it was possible to build a own kitchen using modular objects (instead of one finished kitchen model). This allowed me to position the kitchen objects at specified places for a better game setting. 
At the beginning of the game the user is asked to set up the player settings (system control and symbolic input). Here, a Panel is composite of multiple text and toggle groups for adjust the settings. For the time selection a toggle group of radio buttons (build with two rect transforms) is used. For differentiation from the player gender, the selected field is highlighted in color. The players name can be entered into the input field by the virtual keyboard, which is shown during editing. After all settings have been made, the Play button can be used to change the scene and load the game. Influencing the setting (volume, time) will be implemented in the next step. The next step was to implement the spatial sound. Therefore, Oculus provides a spatializer to adjust the 3D sound setting of the audio .wav files. The audio files where either opensource sound files or generated by a robot voice generator (https://lingojam.com/RobotVoiceGenerator). For the Go-Go hand I've added to both controller a Cube Mesh which is directly attached to the controller. Due to this I have tried to get the current position of the right controller (OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)) and manipulate the movement of the mesh based on the controller movement (ForwardMove.cs). Somehow the controllers position was only recognized when moving through the room with the joystick and not when only moving the controller itself. After some attempts, I have adjusted the technique to a spherecollider gravity pull, since it resulted of the implementation attempts. This allows to point at an object and pulling it to the controller/hand when pulling the triggerIndex. This interaction provides the same goal as the go-go hand, since both have a limitation in distance. Since the shelf is not overloaded there is no danger of occlusion. Besides only the first object which triggers the OnTriggerEnter() will get focussed for the gravity pull. Necessary for this are the DistanceGrabHandRight/Left Prefabs for Controller settings. For this I made use of some pre-setting of the oculus integration. For all objects to be pulled I have create a new Layer in the Unity Editor as Layer 6: grab. This allows that only objects which are assigned to this layer can be pulled towards the controller. 

To enter the scences the must be run independently, since the teleportation between the doors is not implemented yet. 

*Should be more clear. Main pain points: (i) figures or video snippets would have been helpful to aid the understanding of what you described, (ii) formated in a manner, that makes it hard to read (one large block of text), (iii) it is sometimes not clear how the parts of your documentation stand in relation to your concept, (iv) the description of design decisions and how you tackled ocurring problems is limited (more details would have been appropriate).*  

------

## 3. Presentation (20/20 Points)

The presentation  of your intermediate app state will be done in form of a video presentation. You will need to conduct a video recording of your presentation and upload it to ILIAS until the deadline on the due date (24.06.2021, 09:00). 

### Some things that are important:  
+ The total time for presentation is maximum **six** minutes per person! Each presenter will be strictly held to the time limit. If you go over, your grade will be affected.
+ Your presentation should look professional, prepared with presentation software (e.g.,  PowerPoint). 
+ Make all information on the slides well perceivable (avoid tiny images that no one can understand and small text sizes that make text unreadable).
+ Number your slides. 
+ Avoid it to speak to fast and speak clearly. 
+ Avoid overloaded slides and reduce the amount of text to the minimum necessary to convey the information that is essential. 
+ You can find the grading sheet for the presentation for download in ILIAS. 

### Presentation Structure (Your tasks):

The focus of the presentation is your intermediate app state. Your presentation should address the following points: 

*a. Introduction  [approximately 1 minute]*
+ Title slide (your name, topic, matriculation number, etc).
+ Briefly re-introduce the topic of  your project.
+ Give a brief overview on your current intermediate state. As part of this, state which interaction techniques and cues you implemented until the date of the presentation (techniques for 3d manipulation, travel, system control, symbolic input, and wayfinding cues).

*b. Intermediate App State [approximately 4 minutes]*
+ Present your intermediate app state in a logical order (If possible, try to tell a story. You may make use of your original storyboard for inspiration!).
+ Properly narrate and illustrate interactions, so that the viewers of your presentation can easily understand what is happening (e.g., if a button is pressed, an object is rotated, etc.)
+ Make sure that your narration is consistent with what you show (e.g., avoid it that a video goes on, while you still talk about something that is no longer shown in the video).
+ If your app requires a certain setup, briefly mention how the setup is done (e.g., by placing markers).
+ <b>(Very Important!)</b> Make sure to highlight how users can interact when they use the app and how they are supported by cues. This concerns:
    - Interaction techniques for 3d manipulation.
    - Travel techniques and wayfinding cues.
    - Interaction techniques for system control and symbolic input.
+ Provide arguments for important design decisions (why did you decide for option A instead of option B?).
+ Mention problems you ran into and how you solved/addressed them!
    - We do not expect a flawless app as your intermediate state. Just clearly explain if there are open problems. You still have until your final presentation to address existing problems.
    - If you have or had problems:
        - We expect that you have tried solving them by yourself (so show us how you tried to address them) or that you plan to do so in the future (It is ok if you plan to address a problem after the intermediate presentation. Just make sure to clarify what you plan to do and why).
        - If you tried to address a problem, but could not resolve it completely, e.g., because of technical limitations, clarify (i) the problem, (ii) what you did to address it so far, and (iii) why it is a prevailing problem or – after you addressed it to some extent – maybe still a limitation. It might be the case, that one of your colleagues or we can help you to address the problem (further).
        - Finally, if you found a clever solution to a challenging problem, please also share it. This might help others to solve similar problems in their own projects.

 *c. Outlook [approximately 1 minute]*   
+ Show an updated version of your simple gantt chart that shows how you plan to implement the second half of your design concept in unity 3d in the upcoming weeks (as part of exercise 6). You can use the following template as a basis: [https://templates.office.com/en-us/two-month-gantt-chart-tm56247502?omkt=en-001](https://templates.office.com/en-us/two-month-gantt-chart-tm56247502?omkt=en-001)
+ You may remember, that your final design concept needed to include at least **four** interaction techniques or cues that you did not implement as part of the exercises 1-4 in Unity 3d? Based on this, you are required to implement at least  **four** interaction techniques or cues until the *Presentation of the final app state*, which you did not implement in the exercises 1-4 in Unity 3d. The *Presentation of the final app state* is part of exercise 6, submission deadline: 22.07.2021, 09:00. Make sure to consider this when creating the gantt chart. Note, that you already should have implemented at least **two** of the minimum **four** techniques or cues by now (see the description under *"2. Project"* of this exercise sheet, above).
+ You may  add a time buffer (e.g., 3-4 days) in case of unexpected delays.
+ You may highlight important milestones (e.g., the completion of a feature).

The structure above provides a suggestion that you can use as a template for the structuring of your own presentation. You may have to adapt it to live within the six-minute limit.  You must practice your presentation several times to get a smooth and interesting talk and keep within your time limit.

### Presentation Submission

*a. PDF of presentation*
+ Submit your presentation slides in ILIAS in form of a single pdf file into the folder: [https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250489.html](https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250489.html) (*II. Presentation of Intermediate App State/Presentations (Student Submissions)*). 
+ Name the file (in this order, please): `“<course name> - <current semester>_<year> - intermediate app state - <your name>.pdf”`.  If you do not follow the naming schema provided, your grade will be affected.

*b. MP4 Video of presentation*
+ Submit your video in ILIAS in form of a single mp4 file into the folder: [https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250491.html](https://ilias.uni-konstanz.de/ilias/goto_ilias_uni_fold_1250491.html) (*II. Presentation of Intermediate App State/Videos (Student Submissions)*).
+  Name the file (in this order, please): `“<course name> - <current semester>_<year> - intermediate app state - <your name>.mp4”`.  If you do not follow the naming schema provided, your grade will be affected. 
+ You can directly upload videos <= 256 MB to ILIAS. If this should not work, you may upload your video to the nextcloud of the university ([https://cloud.uni-konstanz.de](https://cloud.uni-konstanz.de)) and add a link to your video in ILIAS.
+ For the creation of a remote presentation you may have a look at the following guide (you do not need subtitles!): [href=https://medium.com/sigchi/a-remote-video-presentation-guide-93957c63aa7a](https://medium.com/sigchi/a-remote-video-presentation-guide-93957c63aa7a)
+ We recommend you the use of a resolution of 1920x1080.
+ To include a video of the presenter on your slides is optional (you are free to do so, but it is not a must have). Powerpoint (Office 365) supports this out of the box now: [https://support.microsoft.com/en-us/office/record-a-slide-show-with-narration-and-slide-timings-0b9502c6-5f6c-40ae-b1e7-e47d8741161c#OfficeVersion=Microsoft_365_for_Windows](https://support.microsoft.com/en-us/office/record-a-slide-show-with-narration-and-slide-timings-0b9502c6-5f6c-40ae-b1e7-e47d8741161c#OfficeVersion=Microsoft_365_for_Windows).
+ To decrease the size of your video before its upload you can use handbrake: [https://handbrake.fr](https://handbrake.fr).
